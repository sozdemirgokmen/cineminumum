﻿using Models.SaloonModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Abstracts
{
    public interface ISaloonManager 
    {
        List<SaloonDTO> GetSaloonListByMovieID(Int32 id);
    }
}
