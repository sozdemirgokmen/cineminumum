﻿using Models.TicketModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Abstracts
{
    public interface ITicketManager
    {
        void Add(TicketDTO dto);
        List<String> GetSoldSeatNumbersOnMovieShow(int movieShowId);
    }
}
