﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Abstracts
{
    public interface IManager
    {
        T GetById<T>(object entityId) where T : class;
        void Insert<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        void Delete<T>(object entityId) where T : class;
        void Delete<T>(T entity) where T : class;
    }
}
