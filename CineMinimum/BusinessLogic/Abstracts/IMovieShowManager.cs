﻿using Models.MovieShowModels;
using Models.SaloonModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Abstracts
{
    public interface IMovieShowManager 
    {
        Int16 GetSaloonCapacityByMovieShowID(Int32 id);
        List<MovieShowDTO> GetMovieShowListByMovieID(Int32 id);
    }
}
