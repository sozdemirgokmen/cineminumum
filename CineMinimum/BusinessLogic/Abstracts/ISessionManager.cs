﻿using Models.SessionModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Abstracts
{
    public interface ISessionManager 
    {
        List<SessionDTO> GetSessionListBySaloonID(Int32 id);
    }
}
