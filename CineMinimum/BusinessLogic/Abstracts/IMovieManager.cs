﻿using Models.MovieModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Abstracts
{
    public interface IMovieManager 
    {
        void MySeed();
        List<MovieDetail> GetAllMovieDetail();
        MovieDetail GetById(int movieId);
    }
}
