﻿using BusinessLogic.Abstracts;
using BusinessLogic.Concretes;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Statics
{
    public static class IoC
    {
        private static readonly IWindsorContainer container;
        static IoC()
        {
            container = new WindsorContainer();
        }
        public static void CastleWindsorRegister()
        {
            MovieMngFactory();
            MovieShowMngFactory();
            SaloonMngFactory();
            SessionMngFactory();
            ManagerFactory();
            DtoManagerFactory();
            TicketFactory();
        }
        private static void MovieMngFactory()
        {
            container.Register(
                Component.For<IMovieManager>().ImplementedBy<MovieManager>()
                );
        }
        private static void MovieShowMngFactory()
        {
            container.Register(
                Component.For<IMovieShowManager>().ImplementedBy<MovieShowManager>()
                );
        }
        private static void SaloonMngFactory()
        {
            container.Register(
                Component.For<ISaloonManager>().ImplementedBy<SaloonManager>()
                );
        }
        private static void SessionMngFactory()
        {
            container.Register(
                Component.For<ISessionManager>().ImplementedBy<SessionManager>()
                );
        }
        private static void ManagerFactory()
        {
            container.Register(
                    Component.For<IManager>().ImplementedBy<Manager>()
                    );
        }
        private static void TicketFactory()
        {
            container.Register(
                        Component.For<ITicketManager>().ImplementedBy<TicketManager>()
                        );
        }
        private static void DtoManagerFactory()
        {
            //container.Register(
            //            Component.For<IDtoManager>().ImplementedBy<DtoManager>()
            //            );
        }
        public static T ResolveManager<T>()
        {
            return container.Resolve<T>();
        }
    }
}
