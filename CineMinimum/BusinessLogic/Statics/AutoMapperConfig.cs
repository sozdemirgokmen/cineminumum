﻿using AutoMapper;
using Entities.Concretes;
using Models.CategoryModels;
using Models.CustomerModels;
using Models.MovieModels;
using Models.MovieShowModels;
using Models.SaloonModels;
using Models.SessionModels;
using Models.TicketModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Statics
{
    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            ConfigureMovieMapping();
            ConfigureCategoryMapping();
            ConfigureMovieShowMapping();
            ConfigureSaloonMapping();
            ConfigureSessionMapping();
            ConfigureTicketMapping();
            ConfigureCustomerMapping();
        }

        private static void ConfigureCustomerMapping()
        {
            Mapper.CreateMap<Customer, CustomerDTO>();
            Mapper.CreateMap<CustomerDTO, Customer>();
        }

        private static void ConfigureTicketMapping()
        {
            Mapper.CreateMap<TicketDTO, Ticket>();
            Mapper.CreateMap<Ticket, TicketDTO>();
        }

        private static void ConfigureSessionMapping()
        {
            Mapper.CreateMap<Session, SessionDTO>();
            Mapper.CreateMap<SessionDTO, Session>();
        }

        private static void ConfigureSaloonMapping()
        {
            Mapper.CreateMap<Saloon, SaloonDTO>();
            Mapper.CreateMap<SaloonDTO, Saloon>();
        }

        private static void ConfigureMovieShowMapping()
        {
            Mapper.CreateMap<MovieShow, MovieShowDTO>();
            Mapper.CreateMap<MovieShowDTO, MovieShow>();
        }

        private static void ConfigureCategoryMapping()
        {
            Mapper.CreateMap<Category, CategoryDTO>();
            Mapper.CreateMap<CategoryDTO, Category>();
        }

        private static void ConfigureMovieMapping()
        {
            Mapper.CreateMap<Movie, MovieDetail>();
            Mapper.CreateMap<MovieDetail, Movie>();
        }
    }
}
