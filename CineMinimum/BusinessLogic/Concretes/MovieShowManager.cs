﻿using AutoMapper;
using BusinessLogic.Abstracts;
using BusinessLogic.Statics;
using DataAccess.Context;
using Entities.Concretes;
using Models.MovieModels;
using Models.MovieShowModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.SaloonModels;

namespace BusinessLogic.Concretes
{
    class MovieShowManager : Manager, IMovieShowManager
    {
        private static String ErrPrefix = "MovieShowManager";
        public MovieShowManager()
            : base(new CinemaContext())
        {
        }
        public List<MovieShowDTO> GetMovieShowListByMovieID(int id)
        {
            List<MovieShowDTO> movieShowDtoList = new List<MovieShowDTO>();

            try
            {
                DbSet<Movie> tmpMovieList = db.Movies;
                DbSet<MovieShow> tmpMovieShowList = db.MovieShows;

                List<MovieShow> movieShowList = tmpMovieList
                        .Join(
                            tmpMovieShowList,
                            movie => movie.ID,
                            movieShow => movieShow.MovieID,
                            (movie, movieShow) => new { movie, movieShow }
                         )
                        .Where(r => r.movie.IsDeleted != true && r.movieShow.IsDeleted != true && r.movieShow.MovieID == id)
                        .Select(x => x.movieShow).ToList();

                foreach (MovieShow movieShow in movieShowList)
                {
                    movieShow.Movie = this.GetById<Movie>(id);

                    MovieShowDTO movieShowDto = Mapper.Map<MovieShowDTO>(movieShow);
                    movieShowDto.Saloons = new SaloonManager().GetSaloonListByMovieID(movieShow.MovieID);
                    movieShowDtoList.Add(movieShowDto);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Hata Kodu : " + ErrPrefix + "\nHata Mesajı : " + ex.Message + "\nDetay : GetMovieShowListByMovieID");
            }

            return movieShowDtoList;
        }
        public Int16 GetSaloonCapacityByMovieShowID(int id)
        {
            Int16 capacity;
            try
            {
                capacity = db.MovieShows.Where(x => x.ID == id).Select(x => x.Saloon.Capacity).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Hata Kodu : " + ErrPrefix + "\nHata Mesajı : " + ex.Message + "\nDetay : GetSaloonCapacityByMovieShowID");
            }

            return capacity;
        }
    }
}
