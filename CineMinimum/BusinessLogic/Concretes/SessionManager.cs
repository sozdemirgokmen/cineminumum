﻿using AutoMapper;
using BusinessLogic.Abstracts;
using BusinessLogic.Statics;
using DataAccess.Context;
using Entities.Concretes;
using Models.SessionModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Concretes
{
     class SessionManager : Manager, ISessionManager
    {
         private static String ErrPrefix = "SessionManager";
        public SessionManager():base(new CinemaContext())
        {
        }

        public List<SessionDTO> GetSessionListBySaloonID(int id)
        {
            List<SessionDTO> sessionDtoList = new List<SessionDTO>();

            try
            {
                DbSet<Session> tmpSessionList = db.Sessions;
                DbSet<MovieShow> tmpMovieShowList = db.MovieShows;

                var tmpObjList = tmpSessionList
                        .Join(
                            tmpMovieShowList,
                            session => session.ID,
                            movieShow => movieShow.SessionID,
                            (session, movieShow) => new { session, movieShow }
                         )
                        .Where(r => r.session.IsDeleted != true && r.movieShow.IsDeleted != true && r.movieShow.SaloonID == id)
                        .Select(x => new { x.session, x.movieShow }).ToList();

                foreach (var tmpObj in tmpObjList)
                {
                    SessionDTO sessionDto = Mapper.Map<SessionDTO>(tmpObj.session);
                    sessionDto.MovieShowID = tmpObj.movieShow.ID;
                    sessionDtoList.Add(sessionDto);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Hata Kodu : " + ErrPrefix + "\nHata Mesajı : " + ex.Message + "\nDetay : GetSessionListBySaloonID");
            }           

            return sessionDtoList;
        }
    }
}
