﻿using AutoMapper;
using BusinessLogic.Abstracts;
using BusinessLogic.Statics;
using DataAccess.Context;
using Entities.Concretes;
using Models.SaloonModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Concretes
{
    class SaloonManager : Manager, ISaloonManager
    {
        private static String ErrPrefix = "SaloonManager";
        public SaloonManager()
            : base(new CinemaContext())
        {
        }
        public List<SaloonDTO> GetSaloonListByMovieID(int id)
        {
            List<SaloonDTO> saloonDtoList = new List<SaloonDTO>();
            List<Int32> saloonIdListForMovie = new List<Int32>();

            try
            {
                List<Saloon> tmpSaloonList = db.Saloons.ToList();
                List<MovieShow> tmpMovieShowList = db.MovieShows.Where(x => x.MovieID == id).ToList();

                foreach (var item in tmpMovieShowList)
                {
                    if (!saloonIdListForMovie.Contains(item.SaloonID))
                    {
                        saloonIdListForMovie.Add(item.SaloonID);
                    }
                }

                foreach (Saloon saloon in tmpSaloonList)
                {
                    if (saloonIdListForMovie.Contains(saloon.ID))
                    {
                        SaloonDTO saloonDto = Mapper.Map<SaloonDTO>(saloon);
                        saloonDto.Sessions = IoC.ResolveManager<ISessionManager>().GetSessionListBySaloonID(saloon.ID);
                        saloonDtoList.Add(saloonDto);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Hata Kodu : " + ErrPrefix + "\nHata Mesajı : " + ex.Message + "\nDetay : GetSaloonListByMovieID");
            }

            return saloonDtoList;
        }
    }
}
