﻿using AutoMapper;
using BusinessLogic.Abstracts;
using DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Concretes
{

    class DtoManager : Manager, IDtoManager
    {
        public DtoManager()
            : base(new CinemaContext())
        {

        }
    }
}
