﻿using BusinessLogic.Abstracts;
using DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Concretes
{
    class Manager : IManager
    {
        private static String ErrPrefix = "Manager";
        protected CinemaContext db;
        public Manager(CinemaContext context)
        {
            db = context;
        }
        public T GetById<T>(object entityId) where T : class
        {
            T retObj;
            try
            {
                DbSet<T> objTable = db.Set<T>();
                db.Entry(objTable.Find(entityId)).State = EntityState.Unchanged;
                retObj = objTable.Find(entityId);
            }
            catch (Exception ex)
            {
                throw new Exception("Hata Kodu : " + ErrPrefix + "\nHata Mesajı : " + ex.Message + "\nDetay : GetById<T>");
            }
            return retObj;
        }
        public void Insert<T>(T entity) where T : class
        {
            try
            {
                DbSet<T> objTable = db.Set<T>();
                objTable.Add(entity);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Hata Kodu : " + ErrPrefix + "\nHata Mesajı : " + ex.Message + "\nDetay : Insert<T>");
            }
        }
        public void Update<T>(T entity) where T : class
        {
            try
            {
                DbSet<T> objTable = db.Set<T>();
                objTable.Attach(entity);
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Hata Kodu : " + ErrPrefix + "\nHata Mesajı : " + ex.Message + "\nDetay : Update<T>");
            }
        }
        public void Delete<T>(object entityId) where T : class
        {
            T entity = GetById<T>(entityId);
            Delete<T>(entity);
        }
        public void Delete<T>(T entity) where T : class
        {
            try
            {
                if (db.Entry(entity).State == EntityState.Detached)
                {
                    DbSet<T> objTable = db.Set<T>();
                    objTable.Attach(entity);
                }
                db.Entry(entity).State = EntityState.Deleted;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Hata Kodu : " + ErrPrefix + "\nHata Mesajı : " + ex.Message + "\nDetay : Delete<T>");
            }
        }
    }
}
