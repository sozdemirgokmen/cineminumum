﻿using AutoMapper;
using BusinessLogic.Abstracts;
using DataAccess.Context;
using Entities.Concretes;
using Models.MovieModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Statics;

namespace BusinessLogic.Concretes
{
    class MovieManager : Manager, IMovieManager
    {
        private static String ErrPrefix = "MovieManager";
        public MovieManager()
            : base(new CinemaContext())
        {
        }
        public void MySeed()
        {
            /*
                 * Başlangıç Datalarını Yükle
                 */
            List<Movie> defaultMovies = new List<Movie>();
            //Kategoriler

            List<Category> defaultCategories = new List<Category>() { 
            new Category(){CategoryName = "Bilim-Kurgu", InsertDate = DateTime.Now},
            new Category(){CategoryName = "Komedi", InsertDate = DateTime.Now},
            new Category(){CategoryName = "Dram", InsertDate = DateTime.Now},
            new Category(){CategoryName = "Gerilim", InsertDate = DateTime.Now},
            new Category(){CategoryName = "Aksiyon", InsertDate = DateTime.Now}
            };

            foreach (Category category in defaultCategories)
                db.Categories.Add(category);

            //Filmler

            defaultMovies = new List<Movie>() { 
                new Movie(){
                    MovieName = "Akıl Oyunları",
                    DisplayDate = new DateTime(2016,05,27), 
                    InsertDate = DateTime.Now, 
                    Categories = new List<Category>(){defaultCategories[0], defaultCategories[2]},
                    ImagePath = "Content/img/beatiful-mind.jpg"
                },
                new Movie(){
                    MovieName = "Üç Ahmak",
                    DisplayDate = new DateTime(2016,05,27), 
                    InsertDate = DateTime.Now,
                    Categories = new List<Category>(){defaultCategories[1], defaultCategories[2]},
                    ImagePath = "Content/img/3-Idiots.jpg"
                },
                new Movie(){
                    MovieName = "Zindan Adası",
                    DisplayDate = new DateTime(2016,05,27), 
                    InsertDate = DateTime.Now,
                    Categories = new List<Category>(){defaultCategories[2], defaultCategories[3]},
                    ImagePath = "Content/img/shutter-island.jpg"
                },
                new Movie(){
                    MovieName = "12 Kızgın Adam",
                    DisplayDate = new DateTime(2016,05,27), 
                    InsertDate = DateTime.Now,
                    Categories = new List<Category>(){defaultCategories[2]},
                    ImagePath = "Content/img/time-crimes.jpg"
                },
                new Movie(){
                    MovieName = "Parelel Yaşam",
                    DisplayDate = new DateTime(2016,05,27),                     
                    InsertDate = DateTime.Now,
                    Categories = new List<Category>(){defaultCategories[0], defaultCategories[3], defaultCategories[4]},
                    ImagePath = "Content/img/parelel-life.jpg"
                }
            };

            foreach (Movie movie in defaultMovies)
                db.Movies.Add(movie);

            db.SaveChanges();
        }
        public List<MovieDetail> GetAllMovieDetail()
        {
            List<MovieDetail> movieDetailList = new List<MovieDetail>();

            try
            {
                List<Movie> movieList = db.Movies.Where(x => x.IsDeleted == false).ToList();

                foreach (Movie movie in movieList)
                {
                    MovieDetail movieDetail = Mapper.Map<MovieDetail>(movie);
                    movieDetail.GenerateCategoriesView();
                    movieDetailList.Add(movieDetail);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Hata Kodu : " + ErrPrefix + "\nHata Mesajı : " + ex.Message + "\nDetay : GetAllMovieDetail");
            }

            return movieDetailList;
        }


        public MovieDetail GetById(int movieId)
        {
            try
            {
                Movie movie = GetById<Movie>(movieId);
                return Mapper.Map<MovieDetail>(movie);
            }
            catch (Exception ex)
            {
                throw new Exception("Hata Kodu : " + ErrPrefix + "\nHata Mesajı : " + ex.Message + "\nDetay : GetById"); 
            }            
        }
    }
}
