﻿using AutoMapper;
using BusinessLogic.Abstracts;
using DataAccess.Context;
using Entities.Concretes;
using Models.TicketModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Concretes
{
    class TicketManager : Manager, ITicketManager
    {
        private static String ErrPrefix = "TicketManager";
        public TicketManager()
            : base(new CinemaContext())
        {
        }

        public void Add(TicketDTO dtoTicket)
        {
            Ticket ticket = Mapper.Map<Ticket>(dtoTicket);
            Insert<Ticket>(ticket);
        }
        public List<String> GetSoldSeatNumbersOnMovieShow(int movieShowId)
        {
            try
            {
                return db.Tickets
                .Where(x => x.MovieShowID == movieShowId)
                .Select(x => x.SeatNo)
                .ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Hata Kodu : " + ErrPrefix + "\nHata Mesajı : " + ex.Message + "\nDetay : GetSoldSeatNumbersOnMovieShow");
            }
        }
    }
}
