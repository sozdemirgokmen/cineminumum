﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MovieShow.aspx.cs" Inherits="CineMimimumUI.MovieShow" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <form id="form1" runat="server">
        <h1><%=ViewState["movie-name"] %></h1>
        <img width="100" height="150" src="<%=ViewState["movie-image"] %>" alt="Film Resmi" />
        <asp:Repeater ID="rptSaloonsForMovie" runat="server">
            <ItemTemplate>
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title"><%# Eval("SaloonName")%></h3>
                        <p><b><%# Eval("Capacity") %></b></p>
                    </div>
                    <div class="panel-body">
                        <asp:Repeater ID="rptSessionsForSaloon" runat="server" DataSource='<%#LoadSessionsForSaloonId((int)Eval("ID"))%>'>
                            <ItemTemplate>
                                <asp:LinkButton CssClass="btn btn-info" Text='<%# ((DateTime)Eval("Time")).TimeOfDay %>' OnCommand="Unnamed_Command" CommandArgument='<%#Eval("MovieShowID")%>' runat="server" />
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </form>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScript" runat="server">
</asp:Content>
