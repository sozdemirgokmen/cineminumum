﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Ticket.aspx.cs" Inherits="CineMimimumUI.Ticket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <form id="form1" runat="server">
        <asp:Label ID="Label1" runat="server" Text="Label">Bilet Fiyatı : 13,00 TL</asp:Label>
        <div class="row">
            <div class="form-group col-md-6">
                <label>Müşteri Adı : </label>
                <asp:TextBox CssClass="form-control" runat="server" ID="txtFirstName" />
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label>Müşteri Soyadı : </label>
                <asp:TextBox CssClass="form-control" runat="server" ID="txtLastName" />
            </div>
        </div>
        <asp:Button OnClick="btnAdd_Click" CssClass="btn btn-success" Text="Bilet Al" ID="btnAdd" runat="server" />
    </form>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScript" runat="server">
</asp:Content>
