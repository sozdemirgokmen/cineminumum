﻿using BusinessLogic.Abstracts;
using BusinessLogic.Statics;
using Models.MovieModels;
using Models.MovieShowModels;
using Models.SaloonModels;
using Models.SessionModels;
using Models.TicketModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CineMimimumUI
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //IoC.ResolveManager<IMovieManager>().MySeed();
            List<MovieDetail> movieDetailList = IoC.ResolveManager<IMovieManager>().GetAllMovieDetail();

            rptMovieDetailList.DataSource = movieDetailList;
            rptMovieDetailList.DataBind();
        }
        protected void link_session_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("MovieShow.aspx?movieid=" + e.CommandArgument);
        }
    }
}