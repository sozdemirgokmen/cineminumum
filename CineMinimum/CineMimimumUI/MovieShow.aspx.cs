﻿using BusinessLogic.Abstracts;
using BusinessLogic.Statics;
using Models.MovieModels;
using Models.MovieShowModels;
using Models.SaloonModels;
using Models.SessionModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CineMimimumUI
{
    public partial class MovieShow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Int32 movieId = Convert.ToInt32(Request.QueryString["movieId"]);
            MovieDetail movie = IoC.ResolveManager<IMovieManager>().GetById(movieId);
            ViewState["movie-name"] = movie.MovieName;
            ViewState["movie-image"] = movie.ImagePath;

            List<SaloonDTO> saloonDtoList = IoC.ResolveManager<ISaloonManager>().GetSaloonListByMovieID(movieId);
            rptSaloonsForMovie.DataSource = saloonDtoList;
            rptSaloonsForMovie.DataBind();
        }
        public List<SessionDTO> LoadSessionsForSaloonId(int saloonId)
        {
            return IoC.ResolveManager<ISessionManager>().GetSessionListBySaloonID(saloonId);
        }
        protected void Unnamed_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("Seat.aspx?movie-show-id=" + e.CommandArgument);
        }
    }
}