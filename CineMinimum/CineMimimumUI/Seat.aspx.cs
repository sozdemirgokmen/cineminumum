﻿using BusinessLogic.Abstracts;
using BusinessLogic.Statics;
using Models.SaloonModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CineMimimumUI
{
    public partial class Seat : System.Web.UI.Page
    {
        public int movieShowId
        {
            get
            {
                return Convert.ToInt32(Request.QueryString["movie-show-id"]);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string[] seatLetters = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T" };

            Int16 saloonCapacity = IoC.ResolveManager<IMovieShowManager>().GetSaloonCapacityByMovieShowID(movieShowId);
            saloonCapacity = (Int16)(saloonCapacity / 10);

            List<String> soldSeatNumbers = IoC.ResolveManager<ITicketManager>().GetSoldSeatNumbersOnMovieShow(movieShowId);

            for (int i = 0; i < saloonCapacity; i++)
            {
                for (int j = 0; j <= 10; j++)
                {
                    Button seat = new Button();
                    seat.ID = "seat" + i + j;
                    seat.Width = 60;
                    seat.Height = 30;
                    if (j != 0)
                    {
                        seat.Text = (seatLetters[i] + j.ToString()).ToString();
                        if (soldSeatNumbers.Contains(seat.Text))
                            seat.BackColor = Color.Gray;
                        else
                        {
                            seat.BackColor = Color.GreenYellow;
                            seat.Command += seat_Command;
                        }
                    }
                    else
                    {
                        seat.Text = (seatLetters[i]).ToString();
                    }
                    Panel1.Controls.Add(seat);
                }
                Panel1.Controls.Add(new LiteralControl("<BR>"));
            }
        }
        void seat_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("Ticket.aspx?movie-show-id=" + movieShowId + "&seat-no=" + ((Button)sender).Text);
        }
    }
}