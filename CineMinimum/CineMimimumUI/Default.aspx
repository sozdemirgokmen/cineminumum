﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CineMimimumUI.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div class="col-md-6 col-md-offset-3">
        <form id="form1" runat="server">
            <asp:Repeater ID="rptMovieDetailList" runat="server">
                <ItemTemplate>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title"><%# Eval("MovieName")%></h3>
                            <p><b><%# Eval("DisplayDate") %></b></p>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-6">
                                <img width="100" height="150" src="<%# Eval("ImagePath")%>" alt="<%# Eval("MovieName")%>" />
                            </div>
                            <div class="col-md-6">
                                <h4><b><%# Eval("CategoriesView") %></b></h4>
                            </div>
                        </div>
                        <div id="div_deneme" class="panel-footer">
                            <asp:LinkButton CssClass="btn btn-info" Text="Seanslar" OnCommand="link_session_Command" CommandArgument='<%#Eval("ID")%>' runat="server" />
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </form>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScript" runat="server">
</asp:Content>
