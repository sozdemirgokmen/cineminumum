﻿using BusinessLogic.Abstracts;
using BusinessLogic.Statics;
using Models.CustomerModels;
using Models.MovieShowModels;
using Models.TicketModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CineMimimumUI
{
    public partial class Ticket : System.Web.UI.Page
    {
        Int32 movieShowId
        {
            get
            {
                return Convert.ToInt32(Request.QueryString["movie-show-id"]);
            }
        }
        String seatNo
        {
            get
            {
                return Request.QueryString["seat-no"].ToString();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CustomerDTO customer = new CustomerDTO();
            customer.FirstName = txtFirstName.Text;
            customer.LastName = txtLastName.Text;
            customer.IsDeleted = false;
            customer.InsertDate = DateTime.Now;


            TicketDTO ticket = new TicketDTO();
            ticket.MovieShowID = movieShowId;
            ticket.SeatNo = seatNo;
            ticket.InsertDate = DateTime.Now;
            ticket.IsDeleted = false;
            ticket.Price = 13;
            //ticket.CustomerID = 1;
            ticket.Customer = customer;

            IoC.ResolveManager<ITicketManager>().Add(ticket);
            
        }
    }
}