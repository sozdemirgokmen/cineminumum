﻿using Models.CategoryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.MovieModels
{
    public class MovieDetail
    {
        public Int32 ID { get; set; }
        public String MovieName { get; set; }
        public DateTime DisplayDate { get; set; }
        public String DateView { get; set; }
        public String ImagePath { get; set; }
        public Boolean IsDeleted { get; set; }
        public List<CategoryDTO> Categories { get; set; }
        public String CategoriesView { get; set; }

        public MovieDetail()
        {
            ID = -1;
            MovieName = "";
            DisplayDate = DateTime.MinValue;
            ImagePath = "";
            IsDeleted = false;
            Categories = new List<CategoryDTO>();
            CategoriesView = "";
        }

        public void GenerateCategoriesView()
        {
            foreach (CategoryDTO categoryDto in Categories)
            {
                CategoriesView += categoryDto.CategoryName + ", ";
            }

            DateView = string.Format("Vizyon Tarihi : {0}", DisplayDate.Year);
            CategoriesView = CategoriesView.TrimEnd(',');
        }
    }
}
