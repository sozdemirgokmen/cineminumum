﻿using Models.SessionModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.SaloonModels
{
    public class SaloonDTO
    {
        public Int32 ID { get; set; }
        public String SaloonName { get; set; }
        public Int16 Capacity { get; set; }
        public List<SessionDTO> Sessions { get; set; }

        public SaloonDTO()
        {
            ID = -1;
            SaloonName = "";
            Capacity = 0;
            Sessions = new List<SessionDTO>();
        }
    }
}
