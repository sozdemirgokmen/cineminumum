﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.CustomerModels;
using Models.MovieShowModels;

namespace Models.TicketModels
{
    public class TicketDTO
    {
        public Int32 ID { get; set; }
        public Int32 MovieShowID { get; set; }
        public MovieShowDTO MovieShow { get; set; }
        public String SeatNo { get; set; }
        public CustomerDTO Customer { get; set; }
        public Int32 CustomerID { get; set; }
        public Decimal Price { get; set; }
        public Boolean IsDeleted { get; set; }
        public DateTime InsertDate { get; set; }
    }
}
