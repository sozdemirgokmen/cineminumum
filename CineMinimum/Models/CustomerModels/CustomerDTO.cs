﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.CustomerModels
{
    public class CustomerDTO
    {
        public Int32 ID { get; set; }
        public Boolean IsDeleted { get; set; }
        public DateTime InsertDate { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }

    }
}
