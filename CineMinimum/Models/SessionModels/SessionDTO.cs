﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.SessionModels
{
    public class SessionDTO
    {
        public Int32 ID { get; set; }
        public DateTime Time { get; set; }
        public Int32 MovieShowID { get; set; }

        public SessionDTO()
        {
            ID = -1;
            Time = DateTime.MinValue;
            MovieShowID = -1;
        }
    }
}
