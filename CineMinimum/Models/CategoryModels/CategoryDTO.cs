﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.CategoryModels
{
    public class CategoryDTO
    {
        public Int32 ID { get; set; }
        public String CategoryName { get; set; }
        public String Description { get; set; }

        public CategoryDTO()
        {
            ID = -1;
            CategoryName = "";
            Description = "";
        }
    }
}
