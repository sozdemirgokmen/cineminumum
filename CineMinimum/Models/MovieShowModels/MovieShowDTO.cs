﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.MovieModels;
using Models.SaloonModels;

namespace Models.MovieShowModels
{
    public class MovieShowDTO
    {
        public Int32 ID { get; set; }
        public Int32 MovieID { get; set; }
        public MovieDetail Movie { get; set; }
        public List<SaloonDTO> Saloons { get; set; }

        public MovieShowDTO()
        {
            ID = -1;
            MovieID = -1;
            Movie = new MovieDetail();
            Saloons = new List<SaloonDTO>();
        }

    }
}
